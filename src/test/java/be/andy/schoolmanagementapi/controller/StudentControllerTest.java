package be.andy.schoolmanagementapi.controller;

import be.andy.schoolmanagementapi.model.Person;
import be.andy.schoolmanagementapi.model.Student;
import be.andy.schoolmanagementapi.repository.PersonRepository;
import be.andy.schoolmanagementapi.repository.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class StudentControllerTest {

    @Mock
    private StudentRepository mockStudentRepository;

    @Mock
    private PersonRepository mockPersonRepository;

    @InjectMocks
    private StudentController studentController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllStudents() {
        // Mocking data
        List<Student> students = Arrays.asList(new Student(), new Student());
        when(mockStudentRepository.findAll()).thenReturn(students);

        // Testing
        ResponseEntity<List<Student>> responseEntity = studentController.getAllStudents();
        assertEquals(students, responseEntity.getBody());
        assertEquals(200, responseEntity.getStatusCodeValue());

        verify(mockStudentRepository, times(1)).findAll();
    }

    @Test
    void testSaveStudent() {
        // Mocking data
        Student student = new Student();
        Person person = new Person();
        person.setPersonId(1L);
        student.setPerson(person);

        when(mockPersonRepository.existsById(anyLong())).thenReturn(true);
        when(mockStudentRepository.save(any(Student.class))).thenReturn(student);

        // Testing
        ResponseEntity<Student> responseEntity = studentController.saveStudent(student);
        assertEquals(student, responseEntity.getBody());
        assertEquals(200, responseEntity.getStatusCodeValue());

        verify(mockStudentRepository, times(1)).save(student);
    }

    @Test
    void testSaveStudentFail() {
        // Mocking data
        Student student = new Student();
        Person person = new Person();
        person.setPersonId(1L);
        student.setPerson(person);

        when(mockPersonRepository.existsById(anyLong())).thenReturn(false);

        // Testing
        ResponseEntity<Student> responseEntity = studentController.saveStudent(student);
        assertEquals(400, responseEntity.getStatusCodeValue());

    }

    @Test
    void testGetStudentById() {
        // Mocking data
        Long studentId = 1L;
        Student student = new Student();
        student.setStudentId(studentId);
        when(mockStudentRepository.findById(studentId)).thenReturn(Optional.of(student));

        // Testing
        ResponseEntity<Student> responseEntity = studentController.getStudentById(studentId);
        assertEquals(student, responseEntity.getBody());
        assertEquals(200, responseEntity.getStatusCodeValue());

        verify(mockStudentRepository, times(1)).findById(studentId);
    }

    @Test
    void testUpdateStudent() {
        // Mocking data
        Long studentId = 1L;
        Student existingStudent = new Student();
        existingStudent.setStudentId(studentId);
        Student updatedStudent = new Student();
        updatedStudent.setStudentId(studentId);

        when(mockStudentRepository.findById(studentId)).thenReturn(Optional.of(existingStudent));
        when(mockStudentRepository.save(existingStudent)).thenReturn(updatedStudent);

        // Testing
        ResponseEntity<Student> responseEntity = studentController.updateStudent(studentId, updatedStudent);
        assertEquals(updatedStudent, responseEntity.getBody());
        assertEquals(200, responseEntity.getStatusCodeValue());

        verify(mockStudentRepository, times(1)).findById(studentId);
        verify(mockStudentRepository, times(1)).save(existingStudent);
    }

    @Test
    void testDeleteStudent() {
        // Mocking data
        Long studentId = 1L;
        when(mockStudentRepository.existsById(studentId)).thenReturn(true);

        // Testing
        ResponseEntity<Void> responseEntity = studentController.deleteStudent(studentId);
        assertEquals(204, responseEntity.getStatusCodeValue());

        verify(mockStudentRepository, times(1)).deleteById(studentId);
    }
}
