package be.andy.schoolmanagementapi.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
        info = @Info(
                contact = @Contact(
                        name = "Andy",
                        email = "andyvanccoding@gmail.com"
                ),
                description = "OpenApi documentation for School management api",
                title = "OpenApi specification - Andy",
                version = "1.0"
        ),
        servers = @Server(
                description = "local",
                url = "http://localhost:8080"
        )
)
public class ConfigOpenApi {
}
