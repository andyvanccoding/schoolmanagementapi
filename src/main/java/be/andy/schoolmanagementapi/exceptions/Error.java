package be.andy.schoolmanagementapi.exceptions;

import org.springframework.http.HttpStatus;


public record Error(HttpStatus httpStatus, String message) {
}
