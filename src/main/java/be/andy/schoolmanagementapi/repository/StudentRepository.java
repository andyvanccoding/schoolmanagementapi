package be.andy.schoolmanagementapi.repository;

import be.andy.schoolmanagementapi.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {

}
