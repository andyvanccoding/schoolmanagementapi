package be.andy.schoolmanagementapi.repository;

import be.andy.schoolmanagementapi.model.CourseAssignment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseAssignmentRepository extends JpaRepository<CourseAssignment, Long> {
}
