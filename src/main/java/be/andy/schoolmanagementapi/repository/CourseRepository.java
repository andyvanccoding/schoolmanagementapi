package be.andy.schoolmanagementapi.repository;

import be.andy.schoolmanagementapi.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
