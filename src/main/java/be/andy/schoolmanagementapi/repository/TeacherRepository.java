package be.andy.schoolmanagementapi.repository;

import be.andy.schoolmanagementapi.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
