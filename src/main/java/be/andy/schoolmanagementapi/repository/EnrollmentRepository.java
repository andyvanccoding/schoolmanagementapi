package be.andy.schoolmanagementapi.repository;

import be.andy.schoolmanagementapi.model.Enrollment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {
}
