package be.andy.schoolmanagementapi.repository;

import be.andy.schoolmanagementapi.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {

}
