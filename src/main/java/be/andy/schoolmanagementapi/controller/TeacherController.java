package be.andy.schoolmanagementapi.controller;

import be.andy.schoolmanagementapi.model.Teacher;
import be.andy.schoolmanagementapi.repository.PersonRepository;
import be.andy.schoolmanagementapi.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    private TeacherRepository repoTeacher;

    @Autowired
    private PersonRepository repoPerson;

    @GetMapping
    public ResponseEntity<List<Teacher>> getAllTeachers() {
        return ResponseEntity.ok(repoTeacher.findAll());
    }

    @PostMapping
    public ResponseEntity<Teacher> saveTeacher(@RequestBody Teacher person) {
        if ((Objects.nonNull(person.getTeacherId()) && repoTeacher.existsById(person.getTeacherId()))
                || !repoPerson.existsById(person.getPerson().getPersonId())) {
            return ResponseEntity.badRequest().body(person);
        }
        return ResponseEntity.ok(repoTeacher.save(person));
    }

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Teacher> getTeacherById(@PathVariable Long id) {
        return repoTeacher.findById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id:^\\d+$}")
    public ResponseEntity<Teacher> updateTeacher(@PathVariable Long id, @RequestBody Teacher Teacher) {
        if (id != Teacher.getTeacherId()) {
            return ResponseEntity.badRequest().build();
        }
        return repoTeacher.findById(id)
                .map(existingTeacher -> {
                    existingTeacher.setDepartment(Teacher.getDepartment());
                    existingTeacher.setHireDate(Teacher.getHireDate());
                    return repoTeacher.save(existingTeacher);
                })
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id:^\\d+$}")
    public ResponseEntity<Void> deleteTeacher(@PathVariable Long id) {
        if (repoTeacher.existsById(id)) {
            repoTeacher.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping("/{id:^\\d+$}")
    public ResponseEntity<Void> patchTeacher(@PathVariable Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


}
