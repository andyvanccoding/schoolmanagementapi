package be.andy.schoolmanagementapi.controller;

import be.andy.schoolmanagementapi.model.Student;
import be.andy.schoolmanagementapi.repository.PersonRepository;
import be.andy.schoolmanagementapi.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentRepository repoStudent;

    @Autowired
    private PersonRepository repoPerson;

    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents() {
        return ResponseEntity.ok(repoStudent.findAll());
    }

    @PostMapping
    public ResponseEntity<Student> saveStudent(@RequestBody Student person) {
        if ((Objects.nonNull(person.getStudentId()) && repoStudent.existsById(person.getStudentId()))
                || !repoPerson.existsById(person.getPerson().getPersonId())) {
            return ResponseEntity.badRequest().body(person);
        }
        return ResponseEntity.ok(repoStudent.save(person));
    }

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Student> getStudentById(@PathVariable Long id) {
        return repoStudent.findById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id:^\\d+$}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody Student student) {
        if (id != student.getStudentId()) {
            return ResponseEntity.badRequest().build();
        }
        return repoStudent.findById(id)
                .map(existingStudent -> {
                    existingStudent.setEnrollmentDate(student.getEnrollmentDate());
                    return repoStudent.save(existingStudent);
                })
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id:^\\d+$}")
    public ResponseEntity<Void> deleteStudent(@PathVariable Long id) {
        if (repoStudent.existsById(id)) {
            repoStudent.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping("/{id:^\\d+$}")
    public ResponseEntity<Void> patchStudent(@PathVariable Long id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


}
