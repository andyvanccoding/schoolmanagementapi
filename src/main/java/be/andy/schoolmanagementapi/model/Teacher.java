package be.andy.schoolmanagementapi.model;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TEACHERS")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TEACHERID")
    private Long teacherId;

    @OneToOne
    @JoinColumn(name = "PERSONID", nullable = false)
    private Person person;

    @Column(name = "DEPARTMENT", nullable = false, length = 50)
    private String department;

    @Column(name = "HIREDATE", nullable = false)
    private LocalDate hireDate;

    @ManyToMany(mappedBy = "teachers", fetch = FetchType.EAGER)
    private List<Course> courses = new ArrayList<>();

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department != null ? department : "DEFAULT_DEPARTMENT";
    }

    public LocalDate getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDate hireDate) {
        this.hireDate = hireDate;
    }
}

