package be.andy.schoolmanagementapi.model;


import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "COURSES")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COURSEID")
    private Long courseId;

    @Column(name = "COURSENAME", nullable = false, length = 100)
    private String courseName;

    @Column(name = "CREDITS", nullable = false)
    private int credits;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "COURSEASSIGNMENTS",
            joinColumns = @JoinColumn(name = "COURSEID"),
            inverseJoinColumns = @JoinColumn(name = "TEACHERID")
    )
    private Set<Teacher> teachers = new HashSet<>();

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }
}

