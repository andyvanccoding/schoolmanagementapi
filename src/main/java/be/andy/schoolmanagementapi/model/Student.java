package be.andy.schoolmanagementapi.model;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "STUDENTS")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STUDENTID")
    private Long studentId;

    @OneToOne
    @JoinColumn(name = "PERSONID", nullable = false)
    private Person person;

    @Column(name = "ENROLLMENTDATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private LocalDate enrollmentDate;

    @OneToMany(mappedBy = "student")
    private Set<Enrollment> enrollments;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDate getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(LocalDate enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }
}

