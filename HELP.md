# Getting Started

- Postman collection present with filename:
    SchoolmanagementApi.postman_collection.json


### Links

These references should help you:

* http://localhost:8080/swagger-ui/index.html#/
* http://localhost:8080/h2-console/login.jsp?jsessionid=568316545d68ad8160a90cf9e9cf460d
* 
    ![img.png](img.png)



